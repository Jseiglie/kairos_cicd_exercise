import React, { Component } from 'react';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Playing with Docker</h1>
        <p>or is it docker toying me?</p>
      </div>
    );
  }
}

export default App;
